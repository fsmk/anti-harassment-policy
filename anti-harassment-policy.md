# Anti-Harassment Policy

## INDEX

1. Policy Statement
2. Need for the policy
3. Scope
4. Definitions
    1. Harassment
    2. Sexual Harassment
    3. Bullying
    4. Office Space
    5. Abuse of Power
5. Application
    1. Prevention
    2. Rights
    3. Obligations
6. Redressal Committee
7. Procedure for dealing with complaints
    1. Resolution options
    2. Complaints
    3. Mediate
    4. Investigate
    5. Corrective Actions
    6. Appeal
8. Policy implementation and review
9. Disciplinary Action.
10. Confidentiality
11. Access to reports and documents.
12. Protection of Complainant/Victim
13. Annexure

## 1. POLICY STATEMENT

As an organization that believes in equality, we, as Free Software Movement Karnataka (hereafter known as FSMK), are committed to conducting and governing ourselves with values that are reflective of dignity,
transparency, accountability and mutual respect for each other. To achieve this
we have developed governance structures, practices and procedures that ensure
that such conduct at all levels. It is thus
in acknowledgement of and consonance with these values, that we are dedicated
to ensuring that the environment at all our locations(office space, community
center etc,.)  is conducive to fair, safe and harmonious relations based on
mutual trust and respect, between all the associates of the organization. We
also strive to guarantee a safe and welcoming environment to all those who
visit any of our locations in any capacity, such as general public.
Discrimination and harassment of any type is strictly prohibited. We wish to
promote and maintain this culture to ensure that the members of the
organization do not engage in practices that are intimidating in any form or
manner.

We, at FSMK, aim to provide a safe working or interacting environment and
prohibit any form of harassment. Hence any act of harassment or related
retaliation against or by any associate is unacceptable. This policy therefore,
intends to prohibit such occurrence and also details procedures to follow when
an associate believes that a violation of the policy has occurred within the
ambit of all the applicable regulations regarding any type of harassment.

Making a false complaint about harassment or providing false information about
a complaint will also be regarded as a violation of policy.

Violation of this policy will call for strict disciplinary action up to and
including termination.

## 2. NEED FOR THE POLICY

* As an organization that believes in equality and freedom, to put to practice
  these rights in their truest sense so as to enable and provide a platform to
  any member or representative of the member, who has been a victim of
  harassment within the scope of the organization, to bring light to incidents
  that may go against the very idea of these rights.
* To fulfil the directive of the Supreme Court of India enjoining all and to
  develop and implement a Policy against harassment of any gender at an
  organization. The policy will remain applicable wherever member or associates
  have occasion to interact with each other. Including, for example, in
  vehicles, third party premises, off site meetings and public venues.
* To uphold people's right to protection against harassment and the right to
  livelihood, and towards that end for the prevention and redressal of any type
  of Harassment.
* To evolve an effective mechanism for the prevention, prohibition and
  redressal of harassment of any gender at the organization or in the course of
  official duties with the organization.
* To promote an environment that will raise awareness about and deter acts of
  harassment of members of the organization.
* To ensure implementation of the Policy in letter and spirit by taking all
  necessary and reasonable steps including but not limited to constitution of
  appropriate committees for purposes of gender sensitization and to conduct
  enquiries into complaints of harassment.
* To uphold the commitment of the organization to provide an environment that
  is free from discrimination and violence based on gender.

## 3. SCOPE

This policy will extend to any individual or organization associating with
FSMK. Including those employed, volunteers and also the core operating members
of FSMK. The scope of the policy will be applicable to the aforementioned
associates for the duration of their association with the organization.

## 4. DEFINITIONS

**Harassment** 

Harassment comprises any unwelcome or objectionable, physical, visual or verbal conduct, comment or display, whether intended or unintended, that is insulting, humiliating or degrading to another person, or creates an intimidating, hostile or offensive environment and/or is on the basis of race, ethnicity, language, financial ability, religion, gender, or sexual orientation, disability or age, or any other kind of discrimination which is prohibited by particular provincial/territorial legislation:

- Made by an employee, volunteer, member or associate of the organization.
- Directed at and offensive to any other employee, volunteer, member, associate of the organization, or any other individual or group that the person knew or reasonably ought to have known would be offensive.

Examples of harassment include, but are not limited to:

- Threats made or perceived, that are malicious, vexatious, or based on any of the prohibited grounds under Human Rights legislation.
- Derogatory written or verbal communication or gestures (e.g. name-calling, slurs, taunting pictures or posters, bullying, graffiti), that are malicious, vexatious or that relate to any of the prohibited grounds under Human Rights Legislation.
- Application of stereotypes or generalizations based on any of the prohibited grounds under the legislation.

**Sexual Harassment**

Sexual harassment means any unwelcome conduct (direct or implied) such as
physical contacts and advances, comment, gestures, demand and request for
sexual favours, sexually cultured remarks, showing pornography, or any
other unwelcome "sexually determined behaviour" (physical, verbal or
nonverbal conduct), whether on a one-time basis or in a continuous series
of incidents that:
* Might reasonably be expected to cause offence, embarrassment or
  humiliation.
* Might reasonably be expected to perceived as placing a condition of a
  sexual nature for employment or on any opportunity for advancement.

Examples of sexual harassment include, but are not limited to:
- Remarks, jokes, innuendoes or other comments regarding someone's body,
  appearance, physical or sexual characteristics or clothing.
- Displaying of sexually offensive or derogatory pictures, cartoons or
  other materials.
- Persistent unwelcome or uninvited invitations or requests.
- Unwelcome questions or sharing of information regarding a person's
  sexuality, sexual activity or sexual orientation.
- Conduct or comments intended to create, or having the effect of, creating
  an intimidating, hostile or offensive environment.

**Bullying**

Bullying is an offensive, cruel, intimidating, insulting or humiliating
behavior which includes physical violence or the threat of physical
violence. It can be physical or verbal, direct or indirect such as gossip.
Bullying is considered harassment in general, unless there is physical
contact or a threat of violence, where it is considered violence. Bullying
is ill treatment which is not addressed under human rights legislation or
criminal codes.

**Office-space**

For the purposes of this policy, office space includes any location in
which associates are engaged in organizational activities necessary to
perform their responsibilities. This includes, but is not limited to,
organization buildings, parking spaces, employee/volunteer organized
events, event locations, individuals' homes, and during travel.

**Abuse of power**

Abuse of power happens whenever an organization employee/volunteer/member
abuses or misuses his/her power and discretion for personal benefit, or in
benefit of another person. Abuse of power, for the purposes of this policy,
includes situations involving a minor, situations that involve a reporting
relationship, or any situation that includes an accusation from an
associate against an employee/volunteer/member who is providing a service
upon which the associate depends. Incidents that involved an abuse of power
are considered by the organization as being, generally, of a more serious
nature than peer-to-peer situations.

## 5. APPLICATION

1. **Prevention**

    Prevention is the first line of defence against occurrences of harassment.
    The organization will take reasonable steps to ensure prevention of
    harassment of any form which may include circulating applicable policies
    and other related information among its associates and incoming new members
    or volunteers. It is necessary and expedient for officer bearers as well as
    other responsible persons to observe certain guidelines to ensure the
    prevention of harassment of any individual irrespective of their gender.
    All associates of the organization are reminded of their obligation to
    follow the organizations conduct and the fundamental principles, and to
    adhere to policies and procedures aiming at ensuring a positive
    environment. There is a duty upon all to prevent harassment by discouraging
    inappropriate activities and by reporting incidents, as per this policy.

2. **Rights**

    Everyone has the right to:
    * An environment that is free from harassment.
    * File a complaint when the environment is not free from harassment.
    * Be informed of complaints made against them.
    * Obtain an investigation of the complaint without fear of embarrassment or
      reprisal.
    * Have a fair hearing.
    * Be kept informed throughout the process and of remedial action taken.
    * A fair appeal process for both the respondent and complainant.
    * Confidentiality to the degree possible under the circumstances.
    * Representation by a third party.

3. **Obligations**

    The people in the organization have the responsibility to ensure safety and
    health of all those who come in contact with the organization, whether the
    contact is with another organization, volunteers, members or employees.

    As an organization, we are obligated to take all complaints seriously by:
    * Using due diligence, which is the obligation to take reasonable measures
      to provide appropriate service.
    * Being very familiar with the harassment policy and following it closely.
    * Following the process without bias.
    * Documenting all information from the first disclosure to the final
      resolution.
    * Recording only relevant facts - no feelings.
    * Signing and dating all documents.
    * Using common sense.

    #### Office bearers, volunteers, members and employees responsibilities

    All office bearers, volunteers, members and employees are responsible for
    contributing to a positive environment and for identifying and
    discouraging comments or activities that are contrary to this policy. This
    includes advising people or the alleged harasser that the behavior is
    unwelcome.

    Where a situation occurs, or where an office bearer, volunteer, member or
    employee believes a situation has occurred, the person is obligated to report
    it to the General Secretary. If a situation occurs which involves the General
    Secretary, or if the General Secretary does not intervene appropriately, the
    individual may report to the Joint Secretary or the Executive Committee of the
    organization.

    #### General Secretary, Joint Secretary and Executive Committee responsibilities

    The General Secretary, Joint Secretary and Executive Committee are expected to
    eliminate any aspects of the environment that are not in keeping with this
    policy, whether or not a complaint has been made. General Secretary, Joint
    Secretary and Executive Committee are obligated to implement this policy as
    autonomous entities, and with consultation from the appointed member to address
    issues as per this policy.

    **The member or volunteer or employee assigned to address issues as per this**
    **policy will**:
    * Advice parties(complainants and the accused) of the process.
    * Facilitate communication between parties with a view of resolving conflict.
    * Coach parties as required.
    * Ensure that the process is followed within the prescribed time frame.
    * Arrange for investigation, mediation and expertise, as required.
    * Coordinate the follow up actions.
    * Maintain original copies of all documentation.
    * Educate all associates on the application of this policy.

    **General Secretary, Joint Secretary and the Executive Committee will assign the**
    **role of a mediator who shall**:
    - Determine the parties' wishes and needs.
    - Establish the ground rules for the discussions, with all parties agreeing to the rules.
    - Explore with parties different ways to get their individual needs and thus resolve the complaint.

    **General Secretary, Joint Secretary and Executive Committee will assign the role**
    **of an investigator who shall**:
    - Promptly make arrangements for a thorough and unbiased investigation to be conducted in a timely and confidential manner as possible.
    - Inform all parties on their rights and responsibilities.
    - Secure all complaints and responses in writing, with dates, names, witnesses and full description of the incident(s).
    - Interview the parties involved and any witnesses to the behaviour, if necessary.
    - Notify any individual interviewed of their right to be accompanied by a representative of their choice.
    - Keep the parties informed during the process, including providing the alleged harasser with full particulars of the allegations and a copy of the written complaint.
    - An individual accused of discrimination or harassment will be entitled to respond to the complaint and may wish to offer their perspective regarding the allegations and/or present a proposal for resolution.
    - Prepare a report to present to the General Secretary, Joint Secretary and Executive Committee outlining the allegations of the complainant, the response of the alleged harasser, the evidence of any witnesses, and the conclusion reached.

    **The General Secretary shall**:
    - Keep a confidential record of the number of complaints filed, the nature of the complaints, the outcome of the investigation and the type of corrective action taken.

## 6. REDRESSAL COMMITTEE

The organization may opt to create a redressal committee, for a given term, to
specifically address any complaints regarding harassment. The committee will be
nominated by the Executive Committee not necessarily from within it and will consist of equal
ratio of all genders. The organization may also opt to create redressal
committee as incidents occur and nominate individual(s) from within the
organization to receive complaints from the complainant and notify the office
bearers about the same.

## 7. PROCEDURE FOR DEALING WITH COMPLAINTS

Situations where there has been an accusation of harassment are extremely
sensitive and often complex. At all times, the emotional and physical safety of
the complainant is paramount, and this may involve taking steps that are not
outlined in this policy. The organization is committed to providing a
supportive environment to resolve concerns of harassment:

* **Resolution Options** - 
    1. **No Action**: The behaviour is not found to be harassment, and the
    complainant agrees.
    2. **Resolve**: If the harassment is subtle or mild and the complainant agrees,
    the complaint is resolved informally with the assistance of the individual
    nominated for redressal or the redressal committee.
    3. **Action**: If the complaint represents a case of moderate or severe
    harassment or if the incident involves an abuse of power, then the
    individual assigned or the redressal committee should be notified and the
    complaint is taken forward for mediation, investigation or, in extreme
    circumstances, the police.

* **Complaints** -
    1.  A member within the scope of the organization with a harassment
    concern, who is not comfortable with the informal resolution options or has
    exhausted such options, may make a formal complaint to the complaints
    committee.
    2. Where such conduct, on the part of the accused, amounts to a specific
    offence under the law, the organization shall initiate appropriate action
    in accordance with law by making a complaint with the appropriate
    authority.
    3. The Redressal Committee shall conduct such investigations in a timely
    manner and shall submit a written report containing the findings and
    recommendations to the necessary office bearers as soon as practically
    possible.
    4. The EC along with core will ensure corrective action on the
    recommendations of the Redressal Committee and keep the complainant
    informed of the same.

* **Mediate** -

    The alleged harasser will be advised of the complaint, if this has not
    already happened. A mediator will be chosen by the organization or the
    redressal committee, and confirmed, provided the parties to the complaint
    agree. In case of no agreement, alternate names will be considered.
    Mediation takes place and the situation is resolved to the satisfaction of
    both parties. If no agreement for either a name or process is secured, then
    the case is referred to investigation.

* **Investigate** - 

    Where, for whatever reason, the complaint remains unsolved, then the
    assigned individual or the redressal committee, in consultation with the
    General Secretary, chooses an investigator.

    The investigator will conduct a thorough and unbiased investigation and
    provide a written report, including recommendation for action, to the
    assigned individual or the redressal committee, and the General Secretary.
    They will confer with one another, and others as required, and the assigned
    individual or the redressal committee will take appropriate actions.

* **Corrective actions** - 

    When considering the appropriate action, the assigned individual or the
    redressal committee will consider the evidence, the nature of the
    harassment, whether physical contact was involved, whether the situation
    was isolated, and whether there was an abuse of power.

    Corrective action may include any of the following:

    * Formal apology
    * Counseling
    * Spoken / Written warning to the perpetrator
    * The member if found guilty of the offence will be expelled
    * Sensitivity training
    * Referral to police or other legal authorities
    * Other sanctions

* **Appeal** - 

    Either the complainant or alleged harasser may, within thirty(30) days of
    being notified of the action, submit an appeal, in writing, to the General
    Secretary or the redressal committee or anyone who is an office bearer. In
    the event that the General Secretary determines that a further
    investigation is required, any additional findings will be disclosed to the
    parties involved, who will be provided with an opportunity to respond. The
    General Secretary will then review the record  and determine whether or not
    a violation of the policy has occurred.

## 8. POLICY IMPLEMENTATION AND REVIEW

The policy will be implemented and reviewed by the office bearers of the
organization. The organization reserves the right to amend, abrogate, modify,
rescind / reinstate the entire policy or any part of it at any time

## 9. DISCIPLINARY ACTION

Harassment by any individual or an associate of the organization is a serious
offence. If an accusation is substantiated, the harasser will be subject to
immediate disciplinary action, up to and including expel. Intentionally
accusing someone of harassment, known to be false, is a serious offence and
subject to disciplinary action. The organization reserves the right to
discipline those whose complaints are frivolous or vexatious.

Any interference with the conduct of an investigation, or retaliation against a
complainant, respondent or witness, may itself result in disciplinary action.

Criteria in determining level of disciplinary action shall be based on fact
scenario and will take into account harm to the individual, harm to the
organisation and its reputation, whether or not there was an unequal power
relationship.

Where the conduct involves, or may involve, criminal activity, the organisation
reserves the right to invoke criminal charges.

## 10. CONFIDENTIALITY

The organization understands that it is difficult for the victim to come forward with a complaint of sexual/physical harassment and recognizes the victim's interest in keeping the matter confidential.

To protect the interests of the victim, confidentiality of the accused person and others who may report incidents harassment will be maintained throughout the investigatory process to an extent practicable and appropriate under the circumstances.

## 11. ACCESS TO REPORTS AND DOCUMENTS

All records of complaints, including contents of meetings, results of investigations and other relevant material will be kept confidential by the organization except where disclosure is required under disciplinary or other remedial processes.

## 12. PROTECTION OF COMPLAINANT / VICTIM

The organization is committed to ensuring that no member who brings forward a harassment concern is subject to any form of reprisal. Any reprisal will be subject to disciplinary action. The organization will ensure that the victim or witnesses are not victimized or discriminated against while dealing with complaints of harassment. However, anyone who abuses the procedure (for example, by maliciously putting an allegation knowing it to be untrue) will be subject to disciplinary action.

## 13. ANNEXURES

#### 1. ANNEXURE-I RESPONSIBILITY OF ASSOCIATES

All associates of the organization have a personal responsibility to ensure
that their behavior is not contrary to this policy as earlier mentioned.  The
below is, only an indicative and is not limited to, the basic Do's and Dont's:

Do's:

* Be aware of organizations harassment policy.
* Be aware of inappropriate behaviours and avoid the same.
* Say "NO" if asked to go to places, do things or participate in situations
  that make you uncomfortable.
* Trust your instincts and walk away from uncomfortable situations.
* Say "NO" to offensive behaviour as soon as it occurs.
* Refrain from taking discriminatory action or decision which are contrary to
  the spirit of this policy.
* Maintain confidentiality regarding any aspect of an enquiry to which they
  maybe a party to.

Don'ts:

Verbal harassment:

* Sexually suggestive comments or comments on physical attributes.
* Offensive language that insults or demeans a colleague, using terms of
  endearment.
* Singing or humming vulgar songs.
* Requests for sexual favours, sexual advances, coerced acts of sexual nature.
* Requests for dates or repeated pressure for social contact.
* Sexually colored propositions, insults or threats.
* Offensive and persistent risque jokes or kidding about sex or gender-specific
  traits.

Nonverbal harassment:
* Offensive gesture, staring, leering or whistling with an intention to
  discomfort another.
* Sounds, gestures, display of offensive books, pictures, magazines or
  derogatory written material.
* Showing or mailing pornographic posters, or offensive websites, cartoons or
  drawings.
* Suggestive letters, emails, SMS or phone calls.
* Bullying a colleague.
* Touching oneself sexually or persistent and unwelcome flirting.

Physical harassment:
* Physical contact or advances.
* Intentional touching of the body e.g. hugs, kisses, fondling, pinching, etc
  that make others feel uncomfortable.
* Any displays of affection that make others uncomfortable or are inappropriate
  at office, camps, etc.
